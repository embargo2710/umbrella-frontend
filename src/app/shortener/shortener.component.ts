import {Component} from '@angular/core';
import {Url} from '../url'
import {ShortenerService} from "./shortener.service";
import {MyResponse} from "../myResponse";

@Component({
    selector: 'app-shortener',
    templateUrl: './shortener.component.html',
    styleUrls: ['./shortener.component.css'],
    providers: [ShortenerService],
})

export class ShortenerComponent {

    model: Url = {
        full_url: null,
        short_url: null
    };

    response: MyResponse = {
        error: null,
        result: false,
        short_url: null
    };

    constructor(private shortenerService: ShortenerService) {
    }

    onSubmit(form) {
        this.shortenerService.short(this.model).subscribe(
            response => {
                if (response.result) {
                    this.response.short_url = response.short_url;
                    this.response.result = true;
                    this.response.error = null;
                } else {
                    this.response.error = response.error;
                    this.response.result = false;
                }
                form.reset();
            },
            error => {
                this.response.error = 'Хьюстон, у нас проблемы: ' + error.message;
            }
        );
    }

}
