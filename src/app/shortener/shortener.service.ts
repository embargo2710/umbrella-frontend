import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Url} from "../url";
import {Observable} from "rxjs/Observable";
import {MyResponse} from "../myResponse";

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json',
    })
};

@Injectable()
export class ShortenerService {
    constructor(private http: HttpClient) {
    }

    short (model: Url): Observable<MyResponse> {
        // Прописал жестко, но можно мягче
        return this.http.post<MyResponse>('http://api.artika.studio/api/short', model, httpOptions);
    }
}