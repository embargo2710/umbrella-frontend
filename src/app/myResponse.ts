export class MyResponse {
    constructor(public error: string, public result: boolean, public short_url: string) {
    }
}