export class Url {
    constructor(public full_url: string, public short_url: string) {}
}
